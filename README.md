# Dimosu 1.0


Este trabalho foi implementado para o Evento UNIBHParty - Hackathon Primeiro Semestre de 2018, realizado pelo Centro Universitário de Belo Horizonte (UniBH).  

O seu proposito é desenvolver um software capaz de identificar através das câmeras de segurança do COP-BH (Centro Integrado de Operações de Belo Horizonte) a queda de árvores utilizando técnicas de Machine Learning e Deep Learning para fazer o rastreamento dos recursos móveis de órgãos da PBH (Prefeitura de Belo Horizonte), tais como: como viaturas, agentes, e etc., utilizando, por exemplo, o GPS dos rádios de comunicação para identificar qual ou quais agente(s) irá(ão) atender a ocorrência. Todas as ocorrências e agentes deverão ser plotados no mapa. 

O projeto foi desenvolvido pelos alunos:<br>
Carlos Eduardo Capanema <br>
Fernando Marlon da Silva Dutra <br>
Henrique Barcellos Carvalho <br>

É possível verificar o edital [clicando aqui](https://pt.scribd.com/document/381418292/Edital-Hackathon-2018-VF). 

Sinta-se à vontade para baixar ou clonar o código fonte:

```
https://gitlab.com/fernandomsdutra/hackathon.git
```

# Por que utilizar o Dimosu?

**• O Dimosu possui uma interface simples e pode ser utilizado por qualquer pessoa;** <br> <br>
**• Os dados utilizados são dados reais da Prefeitura de Belo Horizonte e confiáveis;** <br> <br>
**• É integrado com a API do Google Maps e sua atualização ocorre em tempo real;** <br> <br>
**• Utiliza somente ferramentas de licença gratuita;** <br> <br>
**• Sua utilização auxilia na locação de recursos de forma assertiva para cada ocorrência, evitando gastos desnecessários com locomoção e alocando somente os agentes necessários para cada ocorrência.** <br> <br>


# Ferramentas utilizadas

A solução foi desenvolvida na linguagem PHP em seu back-end utilizando a ferramenta SublimeText3, para o front-end foi utilizado JavaScript, HTML5 e CSS3.

Para o banco de dados, foi utilizado a linguagem MySQL com o auxilio da ferramenta gratuita phpMyAdmin.

# Preparando o Ambiente

Com as ferramentas devidamente instaladas e configuradas, a primeira etapa foi a realização da conexão com a API do Google Maps via JavaScript, já que a o BhMap ainda não possui todos os recursos necessários para plotar novas informações de maneira orientada e de fácil visualização. Em seguida foi populado o mapa com os registros das árvores de acordo com um modelo de JSON do BHMap.

Através do mapa, também será possível visualizar o agente, porém o mesmo não poderá ser populado da mesma forma que as árvores por não tem localização fixa. 
Eles serão monitorados através do GPS do veículo em conjunto com o smartphone. 
A necessidade da integração do GPS do smartphone com o veículo ocorre por conta da possibilidade de necessidade de um número especifico de agentes para uma determinada ocorrência.

JSON BHMap: 
```
{
    "FID": "",
    "ID_EQ_EL": ,
    "TIPO_LOGRADOURO": "",
    "NOME_LOGRADOURO": "",
    "NUMERO_APROXIMADO": "",
    "ORGAO_RESPONSAVEL": "",
    "LONGITUDE": "",
    "LATITUDE": ""
  }
  ```

**Exigência: Para a solução funcionar no BHMap, é necessário que seja incluído a camada de árvores contendo o registro das árvores da cidade.**

Foi criado um banco de dados para armazenar os registros dos agentes próximos à ocorrência.
Nesse banco de dados deverão estar cadastrado todos os agentes da cidade. 
Assim que houver a identificação da queda de uma árvore, será atualizada a tabela historico_agente inserindo a data, hora, endereço e geolocalização dos agentes próximos à ocorrência.

Estrutura do banco de dados:

![Banco de Dados](https://lh3.googleusercontent.com/-6hz5PQPkQGY/WxyhbVRM_PI/AAAAAAAADgw/ryDqt4Su_lUrWAyVy0iWXeErbVa37p8pwCL0BGAYYCw/h269/2018-06-09.png)

Com a conclusão do desenvolvimento do projeto, geramos uma aplicação web com a exibição dos dados armazenados e plotados no plano de fundo do Google Maps. 

Para simular a queda de uma árvore, criamos o botão "Atualizar" que simula a queda de uma árvore em um ponto aleatório da cidade, com isso, a aplicação localiza todos os agentes em um raio de distância pré-determinado e aciona o mais propício para atender a ocorrência. Permite ainda definir o valor do raio que deseja visualizar e identificar os agentes mais próximos.

Gif explicando o funcionamento:

![Árvores GIF](https://lh3.googleusercontent.com/-Hul8YwC4zKo/WxzSQpF-iyI/AAAAAAAAHy4/bk6FPsMhQzUMVfkPEHlwTInLAV6nCqrEgCL0BGAYYCw/h720/2018-06-10.gif)


# Projeções futuras

Com o curto tempo para desenvolvimento do projeto, fica como projeções futuras a parte de fazer o reconhecimento das imagens tendo como objetivo a identificação de sua queda com as câmeras do COPBH utilizando Deep Learning e Machine Learning. A ideia seria de construir uma base de dados com imagens de árvores e de árvores caídas para fazer o treinamento. Posteriormente a isso, ter o reconhecimento através da câmeras que já existem no COPBH.
 