<?php

// Atribui o conteúdo do arquivo para variável
$arvore = file_get_contents('json/ARVORE.json');
$agente = file_get_contents('json/AGENTE.json');

// Decodifica o formato JSON e retorna um Objeto
$jsonArvore = json_decode($arvore);
$jsonAgente = json_decode($agente);

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Smart City - Dimosu</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta charset="utf-8">
    <style>
      #map {
        height: 100%;
      }
      html, body {
        height: 100%;
        margin: 0;
        padding: 0;
      }
      #legend {
        font-family: Arial, sans-serif;
        background: #fff;
        opacity: 0.8;
        padding: 10px;
        margin: 10px;
        border: 2px solid #000;
      }
      #legend h3 {
        margin-top: 0;
      }
      #legend img {
        vertical-align: middle;
      }
    </style>
  </head>
  <body>
    <button type="button" onclick="deleteMarkers()">Atualizar!</button>
    <label>Raio de atuação:</label>
     <select id="raio">
	  <option value="500">500m</option>
	  <option value="1000">1km</option>
	  <option value="1500">1,5km</option>
	  <option value="2000">2km</option>
	</select> 
    <div id="map"></div>
    <div id="legend"><h3>Legenda</h3></div>
    
    <script src="http://code.jquery.com/jquery-1.11.3.min.js"></script>
    <script>	   	
        var map;
        var markers = [];

      	function initMap() {
        map = new google.maps.Map(document.getElementById('map'), {
          zoom: 16,
          center: new google.maps.LatLng(-19.8157, -43.9542),
          mapTypeId: 'roadmap'
        });

         var icons = {
          arvore: {
            name: 'Árvore',
            icon: 'imagens/arvore.png'
          },
          bhtrans: {
            name: 'BH Trans',
            icon: 'imagens/bhtrans.png'
          },
          defesacivil: {
            name: 'Defesa Civil',
            icon: 'imagens/defesa_civil.png'
          },
          guardamunicipal: {
            name: 'Guarda Municipal',
            icon: 'imagens/guarda_municipal.png'
          },
          samu: {
            name: 'SAMU',
            icon: 'imagens/samu.png'
          }
        };       	

        var obj = <?=json_encode($jsonArvore)?>;	

		for (var data in obj) {	  
			  var marker = new google.maps.Marker({
	          position: new google.maps.LatLng(obj[data].LATITUDE, obj[data].LONGITUDE),
	          title: obj[data].ID_AR,
	          map: map,
	          icon: 'imagens/arvore.png'
	        });	
			  markers.push(marker); 
		}

		var agente = <?=json_encode($jsonAgente)?>;

		for (var data in agente) {  	
			  if (agente[data].ORGAO_RESPONSAVEL == 'GUARDA_MUNICIPAL') {
				  var marker = new google.maps.Marker({
		          position: new google.maps.LatLng(agente[data].LATITUDE, agente[data].LONGITUDE),
		          title: agente[data].ID_AR,
		          map: map,
		          icon: 'imagens/guarda_municipal.png'
		        });	
				  markers.push(marker);
			  } else if (agente[data].ORGAO_RESPONSAVEL == 'BH_TRANS') {
			  	  var marker = new google.maps.Marker({
		          position: new google.maps.LatLng(agente[data].LATITUDE, agente[data].LONGITUDE),
		          title: agente[data].ID_AR,
		          map: map,
		          icon: 'imagens/bhtrans.png'
		        });	
				  markers.push(marker);
			  } else if (agente[data].ORGAO_RESPONSAVEL == 'SAMU') {
			  	  var marker = new google.maps.Marker({
		          position: new google.maps.LatLng(agente[data].LATITUDE, agente[data].LONGITUDE),
		          title: agente[data].ID_AR,
		          map: map,
		          icon: 'imagens/samu.png'
		        });	
				  markers.push(marker);
			  } else if (agente[data].ORGAO_RESPONSAVEL == 'DEFESA_CIVIL') {
			  	  var marker = new google.maps.Marker({
		          position: new google.maps.LatLng(agente[data].LATITUDE, agente[data].LONGITUDE),
		          title: agente[data].ID_AR,
		          map: map,
		          icon: 'imagens/defesa_civil.png'
		        });	
				  markers.push(marker);
			  }			   
		}

		var legend = document.getElementById('legend');
        for (var key in icons) {
          var type = icons[key];
          var name = type.name;
          var icon = type.icon;
          var div = document.createElement('div');
          div.innerHTML = '<img src="' + icon + '"> ' + name;
          legend.appendChild(div);
        }

        map.controls[google.maps.ControlPosition.RIGHT_BOTTOM].push(legend);

      }
    </script>
    <script async defer
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCDE7_SQscvMQLvXe8ENNRGm9vBLC9oKEs&callback=initMap&libraries=geometry">
    </script>
    <script>
    	function clearMarkers() {
        	setMapOnAll(null);
      	}

    	function deleteMarkers() {
	        clearMarkers();
	        markers = [];
	    }

	    function setMapOnAll(map) {
        	for (var i = 0; i < markers.length; i++) {
          		markers[i].setMap(map);
       		}
        	setaIcones();
      	}

      	function rad(x) {return x*Math.PI/180;}
		function find_closest_marker(latitude, longitude, agente, map, raio) {
		    var lat = latitude;
		    var lng = longitude;
		    var R = 6371; // radius of earth in km
		    var distances = [];
		    var closest = -1;
		    var w = 0;	

 			for( i=0;i<agente.length; i++ ) {
 				var arv = new google.maps.LatLng(latitude,longitude);
		    	var agt = new google.maps.LatLng(agente[i].LATITUDE,agente[i].LONGITUDE);

		    	var dist = (google.maps.geometry.spherical.computeDistanceBetween(arv, agt));
   	

				if(dist <= raio) {
					distances[w] = dist;
					w = w + 1;

					if (agente[i].ORGAO_RESPONSAVEL == 'GUARDA_MUNICIPAL') {
		  			var marker = new google.maps.Marker({
		          	position: new google.maps.LatLng(agente[i].LATITUDE, agente[i].LONGITUDE),
		          	title: agente[i].ID_AR,
		          	map: map,
		          	icon: 'imagens/guarda_municipal2.png'
		        	});	
				  	markers.push(marker);
			  		} else if (agente[i].ORGAO_RESPONSAVEL == 'SAMU') {
			  			var marker = new google.maps.Marker({
			          	position: new google.maps.LatLng(agente[i].LATITUDE, agente[i].LONGITUDE),
			          	title: agente[i].ID_AR,
			          	map: map,
			          	icon: 'imagens/samu2.png'
			        	});	
					  markers.push(marker);
			  		} else if (agente[i].ORGAO_RESPONSAVEL == 'DEFESA_CIVIL') {
			  			var marker = new google.maps.Marker({
			          	position: new google.maps.LatLng(agente[i].LATITUDE, agente[i].LONGITUDE),
			          	title: agente[i].ID_AR,
			          	map: map,
			          	icon: 'imagens/defesa_civil2.png'
			        	});	
					  markers.push(marker);
			  		} else if (agente[i].ORGAO_RESPONSAVEL == 'BH_TRANS') {
			  			var marker = new google.maps.Marker({
			          	position: new google.maps.LatLng(agente[i].LATITUDE, agente[i].LONGITUDE),
			          	title: agente[i].ID_AR,
			          	map: map,
			          	icon: 'imagens/bhtrans2.png'
			        	});	
					  markers.push(marker);
			  		}
				}
		    }
		}

		function setaIcones() {

		var obj = <?=json_encode($jsonArvore)?>;	

		var min = Math.ceil(50000);
		var max = Math.floor(51353);
		var aleatorio = Math.floor(Math.random() * (max - min) + min);

		var raio = $("#raio").val();

		for (var data in obj) {

		  if (aleatorio == obj[data].ID_AR) {
			  	var marker = new google.maps.Marker({
	          	position: new google.maps.LatLng(obj[data].LATITUDE, obj[data].LONGITUDE),
	          	title: obj[data].ID_AR,
	          	map: map,
	          	icon: 'imagens/arvore2.png'
	        });	
			  markers.push(marker);		  
        	  map.panTo(marker.position);
        	  map.setZoom(15);
        	  marker.setAnimation(google.maps.Animation.BOUNCE);

        	  var lat = obj[data].LATITUDE;
        	  var long = obj[data].LONGITUDE;  	  

        	  var cityCircle = new google.maps.Circle({
	            strokeColor: '#FF0000',
	            strokeOpacity: 0.8,
	            strokeWeight: 2,
	            fillColor: '#FF0000',
	            fillOpacity: 0.35,
	            map: map,
	            center: new google.maps.LatLng(obj[data].LATITUDE, obj[data].LONGITUDE),
	            radius: parseInt(raio)
	          });

			  } else {
			  	var marker = new google.maps.Marker({
	          	position: new google.maps.LatLng(obj[data].LATITUDE, obj[data].LONGITUDE),
	          	title: obj[data].ID_AR,
	          	map: map,
	          	icon: 'imagens/arvore.png'
	        });	
			  markers.push(marker);
			  }			   
		}	

		var agente = <?=json_encode($jsonAgente)?>;
		find_closest_marker(lat, long, agente, map, raio);

		for (var data in agente) {  	
			  if (agente[data].ORGAO_RESPONSAVEL == 'GUARDA_MUNICIPAL') {
				  var marker = new google.maps.Marker({
		          position: new google.maps.LatLng(agente[data].LATITUDE, agente[data].LONGITUDE),
		          title: agente[data].ID_AR,
		          map: map,
		          icon: 'imagens/guarda_municipal.png'
		        });	
				  markers.push(marker);
			  } else if (agente[data].ORGAO_RESPONSAVEL == 'BH_TRANS') {
			  	  var marker = new google.maps.Marker({
		          position: new google.maps.LatLng(agente[data].LATITUDE, agente[data].LONGITUDE),
		          title: agente[data].ID_AR,
		          map: map,
		          icon: 'imagens/bhtrans.png'
		        });	
				  markers.push(marker);
			  } else if (agente[data].ORGAO_RESPONSAVEL == 'SAMU') {
			  	  var marker = new google.maps.Marker({
		          position: new google.maps.LatLng(agente[data].LATITUDE, agente[data].LONGITUDE),
		          title: agente[data].ID_AR,
		          map: map,
		          icon: 'imagens/samu.png'
		        });	
				  markers.push(marker);
			  } else if (agente[data].ORGAO_RESPONSAVEL == 'DEFESA_CIVIL') {
			  	  var marker = new google.maps.Marker({
		          position: new google.maps.LatLng(agente[data].LATITUDE, agente[data].LONGITUDE),
		          title: agente[data].ID_AR,
		          map: map,
		          icon: 'imagens/defesa_civil.png'
		        });	
				  markers.push(marker);
			  }			   
		}
	}      
      
</script>
  </body>
</html>


